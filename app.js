const xml2js = require("xml2js");
const axios = require("axios").default;
const fs = require("fs");

const url =
  "http://ec.europa.eu/taxation_customs/vies/services/checkVatService";
const args = process.argv;
let maxInstances = args[2];
let startingNumber = args[3];
let numbersPerInstance = args[4];

if (!Number.isInteger(Number(maxInstances))) maxInstances = 1;
if (!Number.isInteger(Number(startingNumber))) startingNumber = 45000000;
if (!Number.isInteger(Number(numbersPerInstance))) numbersPerInstance = 1;

for (let i = 0; i < maxInstances; i++) {
  let min = startingNumber + i * numbersPerInstance;
  let max = min + numbersPerInstance;
  requestHandler(url, "AT", "U", min, max, i);
}

function requestHandler(url, country, prefix, minUid, maxUid, instanceIndex) {
  let xml =
    `<?xml version="1.0" encoding="UTF-8"?>
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
  <soapenv:Header/>
  <soapenv:Body>
      <urn:checkVat>
          <urn:countryCode>` +
    country +
    `</urn:countryCode>
          <urn:vatNumber>` +
    prefix +
    minUid +
    `</urn:vatNumber>
      </urn:checkVat>
  </soapenv:Body>`;

  soapRequest(url, xml)
    .then((response) => {
      if (response && response.response.statusCode == 200) {
        if (minUid < maxUid) {
          console.log("instance: " + instanceIndex + " - uid: " + minUid);
          xml2js.parseString(response.response.body, function (err, result) {
            if (result["soap:Envelope"]) {
              if (result["soap:Envelope"]["soap:Body"][0]["soap:Fault"]) {
                console.error(
                  "\t\t\t\t" +
                    JSON.stringify(
                      result["soap:Envelope"]["soap:Body"][0]["soap:Fault"]
                    )
                );
                requestHandler(
                  url,
                  country,
                  prefix,
                  minUid,
                  maxUid,
                  instanceIndex
                );
              } else {
                console.log(
                  result["soap:Envelope"]["soap:Body"][0][
                    "checkVatResponse"
                  ][0]["valid"][0]
                );

                if (
                  result["soap:Envelope"]["soap:Body"][0][
                    "checkVatResponse"
                  ][0]["valid"][0] == "true"
                ) {
                  fs.appendFile(
                    "uids.txt",
                    JSON.stringify(
                      result["soap:Envelope"]["soap:Body"][0][
                        "checkVatResponse"
                      ][0]
                    ) + "\n",
                    function (err) {
                      if (err) throw err;
                    }
                  );
                }

                requestHandler(
                  url,
                  country,
                  prefix,
                  (minUid += 1),
                  maxUid,
                  instanceIndex
                );
              }
            } else {
              //xml structure changes on IP_BLOCKED
              if (result["soapenv:Envelope"])
                console.error(
                  "\t\t\t\t" +
                    result["soapenv:Envelope"]["soapenv:Body"][0][
                      "soapenv:Fault"
                    ][0]["faultstring"][0]
                );
            }
          });
        } else {
          console.log("FINISHED instance " + instanceIndex);
        }
      } else {
        console.log(
          "repeating request on undefined response (instance: " +
            instanceIndex +
            " uid: " +
            minUid +
            ")"
        );
        requestHandler(url, country, prefix, minUid, maxUid, instanceIndex);
      }
    })
    .catch((error) => {
      console.error(error);
      console.log(
        "repeating request on error (instance: " +
          instanceIndex +
          " uid: " +
          minUid +
          ")"
      );
      requestHandler(url, country, prefix, minUid, maxUid, instanceIndex);
    });
}

async function soapRequest(url, xml) {
  return new Promise((resolve, reject) => {
    axios({
      method: "post",
      url: url,
      data: xml,
    })
      .then((response) => {
        resolve({
          response: {
            headers: response.headers,
            body: response.data,
            statusCode: response.status,
          },
        });
      })
      .catch((error) => {
        console.error(`SOAP FAIL: ${error}`);
        reject(error);
      });
  });
}

function log(instance, iteration, logText) {
  fs.appendFile(
    "log.txt",
    "instance: " +
      instance +
      " - index: " +
      iteration +
      "\t\t" +
      logText +
      "\n",
    function (err) {
      if (err) throw err;
    }
  );
}
