# Start

`npm i` and `node app.js`.

## Input parameters

Additionally you can specify three input parameters

- number of concurrent instances
- starting VAT number
- checks per instance

To start 20 instances starting from 45000000 with 1000 checks per instance type `node app.js 20 45000000 1000`.
